<?php
namespace App;

class Course
{
    private $markBangla;
    private $gradeBangla;

    private $markEnglish;
    private $gradeEnglish;

    private $markMath;
    private $gradeMath;

    public function setMarkBangla($markBangla)
    {
        $this->markBangla = $markBangla;

    }


    public function setGradeBangla()
    {
        $this->gradeBangla = $this->mark2Grade($this->markBangla);
    }



    public function setMarkEnglish($markEnglish)
    {
        $this->markEnglish = $markEnglish;

    }

    public function setGradeEnglish()
    {
        $this->gradeEnglish = $this->mark2Grade($this->markEnglish);
    }

    public function setMarkMath($markMath)
    {
        $this->markMath = $markMath;

    }

    public function setGradeMath()
    {
        $this->gradeMath = $this->mark2Grade($this->markMath);
    }


    public function getMarkBangla()
    {
        return $this->markBangla;
    }

    public function getGradeBangla()
    {
        return $this->gradeBangla;
    }

    public function getMarkEnglish()
    {
        return $this->markEnglish;
    }

    public function getGradeEnglish()
    {
        return $this->gradeEnglish;
    }

    public function getMarkMath()
    {
        return $this->markMath;
    }

    public function getGradeMath()
    {
        return $this->gradeMath;
    }




    public function mark2Grade($mark){


        switch($mark){

            case ($mark>79) :
                $grade = "A+";
                break;

            case ($mark>69) :
                $grade = "A";
                break;

            case ($mark>59) :
                $grade = "A-";
                break;

            case ($mark>49) :
                $grade = "B";
                break;
            case ($mark>39) :
                $grade = "C";
                break;

            case ($mark>32) :
                $grade = "D";
                break;

            default: $grade = "F";

        }



        return $grade;
    }
}